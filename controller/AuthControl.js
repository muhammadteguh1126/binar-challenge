require("dotenv").config();

const { player } = require('../models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const { JWT_SECRET } =  process.env


class Login {
    static async login(req, res, next){
        try {
            const findPlayer = await player.findOne({
                where: { usernames : req.body.usernames }
              })
              if (!findPlayer) {
                throw {
                    status: 401,
                    message: 'Invalid User or Password &&'
                }
            }
            const isPasswordValid = bcrypt.compareSync(req.body.password, findPlayer.password);
            if (!isPasswordValid) {
                throw {
                    status: 401,
                    message: 'Invalid User or Password --'
                }
            }
            const token = jwt.sign({
                id: findPlayer.id,
                username: findPlayer.usernames,
            }, JWT_SECRET);
            res.status(200).json({
                token
            });
        } catch (error) {
          next(error)
        }
    }
}

module.exports = Login