require("dotenv").config();

const { player } = require('../models')
const bcrypt = require('bcrypt')

class Player {
    static async create(req, res, next){
      try {
          const {usernames, password, email} = req.body
          await player.create({
            usernames,
            password: bcrypt.hashSync(password, 10),
            email
          })
          .then(data => {
          return res.status(201).json({
            success: true,
            message: 'Player successfully registered',
            data: {
              usernames: data.usernames
                  }
              })   
          })
          .catch(err => {
            throw {
              status: 400,
              massage: err
            }
          })
      } catch (error){
        next()
      }
    }
    static async list(req, res, next){
      try{
        const Findplayer = await player.findAll({
          attributes: ['usernames', 'email'],
        })
          res.status(200).json(Findplayer
        )
      } catch (error){
        next(error)
      }
    }
    static async getById(req, res, next){
      try{
        const Findplayer = await player.findOne({
          attributes: ['usernames', 'email'],
          where: {
            id: req.params.id
          },
        })      
          if (!Findplayer) {
            throw {
              status: 404,
              message: 'Player not found'
            }
          } else {
              res.status(200).json({ 
                usernames:Findplayer.usernames,
                email:Findplayer.email
            }) }
      } catch (error){
        next(error)
      }
    }
    static async updatePlayer(req, res, next) {
      try {
       await player.update(req.body, {
          where: {
            id: req.params.id
          }
        })
          res.status(200).json({
            message: 'Successfully update'
          })
      } catch(err) {
        next(err)
      }
    }
    static async deletePlayer(req, res, next){
      try{
        const Findplayer = await player.destroy({
          where: {
            id: req.params.id
          },
        })      
          if (!Findplayer) {
            throw {
              status: 404,
              message: 'Player not found'
            }
          } else {
              res.status(200).json({ 
                message: 'Successfully Delete'
            }) }
      } catch (error){
        next(error)
      }
    }
}

module.exports = Player