'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class history_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      history_game.belongsTo(models.match_game, { foreignKey: 'id' })
    }
  }
  history_game.init({
    score: DataTypes.INTEGER,
    history_game: DataTypes.ENUM('win', 'lose')
  }, {
    sequelize,
    modelName: 'history_game',
  });
  return history_game;
};