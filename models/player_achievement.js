'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class player_achievement extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      player_achievement.hasMany(models.player, { foreignKey: 'id' })
      player_achievement.hasMany(models.achievement, { foreignKey: 'id' })
    }
  }
  player_achievement.init({
    player_id: DataTypes.INTEGER,
    achievement_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'player_achievement',
  });
  return player_achievement;
};