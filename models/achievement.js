'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class achievement extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      achievement.belongsTo(models.player_achievement, { foreignKey: 'id' })
    }
  }
  achievement.init({
    nama_achievement: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'achievement',
  });
  return achievement;
};