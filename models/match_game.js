'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class match_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      match_game.hasMany(models.player, { foreignKey: 'id' })
      match_game.hasMany(models.history_game, { foreignKey: 'id' })
    }
  }
  match_game.init({
    player_id: DataTypes.INTEGER,
    history_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'match_game',
  });
  return match_game;
};