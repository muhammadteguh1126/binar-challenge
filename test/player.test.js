const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')
const app = require('../app.js')

let token

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("test123", salt)
    await queryInterface.bulkInsert('players', [
      {
        usernames:"cek",
        email: "cek@mail.com",
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
    token = jwt.sign({
        id: 1,
        usernames: 'cek'
      }, 'coba123')
  })

afterEach(async () => {
await queryInterface.bulkDelete('players', {}, { truncate: true, restartIdentity: true })
})

describe('Create Player', () => {
    it('Success Create', (done) => {
      request(app)
      .post('/player')
      .send({
        usernames:"cek",
        email: "cek@mail.com",
        password: "test123",
        createdAt: new Date(),
        updatedAt: new Date()
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(201)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Player successfully registered')
          done()
        }
      })
    })

    it('Cant Null', (done) => {
        request(app)
        .post('/player')
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(422)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message.includes('Usernames is required')).toBe(false)
            done()
          }
        })
      })
    
      it('Email Must be a valid email address', (done) => {
        request(app)
        .post('/player')
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(422)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message.includes('Must Be a valid email address')).toBe(false)
            done()
          }
        })
      })
})

describe('Get Player ID', () => {
    it('Success', (done) => {
      request(app)
      .get('/player/1')
      .set('Authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('usernames')
          expect(res.body).toHaveProperty('email')
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .get('/player/1')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Authorization denied')
          done()
        }
      })
    })
    // it('Invalid token', (done) => {
    //   request(app)
    //   .get('/player/1')
    //   .set('Authorization', 'coba123')
    //   .end((err, res) => {
    //     if (err) {
    //       done(err)
    //     } else {
    //       expect(res.status).toBe(401)
    //       expect(res.body).toHaveProperty('message')
    //       expect(res.body.message).toBe('Authorization denied')
    //       done()
    //     }
    //   })
    // })
    it('Not found player', (done) => {
        request(app)
        .get('/player/1000')
        .set('Authorization', token)
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(404)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Player not found')
            done()
          }
        })
      })
})

describe('Get Player List', () => {
    it('Success', (done) => {
      request(app)
      .get('/player')
      .set('Authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(Array.isArray(res.body)).toBe(true)
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .get('/player')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Authorization denied')
          done()
        }
      })
    })
})

describe('Edit Player', () => {
    it('Success', (done) => {
      request(app)
      .put('/player/2')
      .set('Authorization', token)
      .send({
        email: "cekUpdate@mail.com",
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Successfully update')
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .put('/player/2')
      .send({
        email: "cekUpdate@mail.com",
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Authorization denied')
          done()
        }
      })
    })
    it('Invalid Token', (done) => {
        request(app)
        .put('/player/1000')
        .set('Authorization', "coba123")
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(500)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('jwt malformed')
            done()
          }
        })
      })
})

describe('Delete Player', () => {
    it('Success', (done) => {
      request(app)
      .delete('/player/1')
      .set('Authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Successfully Delete')
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .delete('/player/1')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Authorization denied')
          done()
        }
      })
    })
    it('Not found player', (done) => {
        request(app)
        .delete('/player/1000')
        .set('Authorization', token)
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(404)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Player not found')
            done()
          }
        })
      })
})