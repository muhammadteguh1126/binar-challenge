const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const bcrypt = require('bcrypt')
const app = require('../app.js')

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("test123", salt)
    await queryInterface.bulkInsert('players', [
      {
        usernames:"cek",
        email: "cek@mail.com",
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  })
  
afterEach(async () => {
await queryInterface.bulkDelete('players', {}, { truncate: true, restartIdentity: true })
})

describe('Login', () => {
    it('Success Login', (done) => {
        request(app)
        .post('/login')
        .send({
            usernames: "cek",
            password: "test123"
        })
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('token')
            done()
            }
        })
    })

    it('Wrong password', (done) => {
        request(app)
        .post('/login')
        .send({
            usernames: "cek",
            password: "test12"
        })
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Invalid User or Password --')
            done()
            }
        })
    })

    it('Wrong usernames', (done) => {
        request(app)
        .post('/login')
        .send({
            usernames: "ceek",
            password: "test123"
        })
        .end((err, res) => {
          if (err) {
            done(err)
          } else {
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('message')
            expect(res.body.message).toBe('Invalid User or Password &&')
            done()
            }
        })
    })

})