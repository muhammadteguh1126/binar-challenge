const express = require('express')
const app = express()
const playerRoutes = require('./routes/NewRoute')
const errorRoutes = require('./middleware/errorHandler');


app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.use(playerRoutes)
app.use(errorRoutes)


module.exports = app