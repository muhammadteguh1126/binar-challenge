const express = require("express");
const router = express.Router();
const {validate, CreateRules} = require('../middleware/validator')
const Auth = require('../middleware/Auth')
const AuthController = require('../controller/AuthControl')
const GameController = require('../controller/NewControl')

router.get('/player', Auth,GameController.list)

router.get('/player/:id',Auth, GameController.getById)

router.post('/player',CreateRules(),validate,GameController.create)

router.post('/login', AuthController.login)

router.put('/player/:id',Auth, GameController.updatePlayer)

router.delete('/player/:id',Auth, GameController.deletePlayer)

module.exports = router