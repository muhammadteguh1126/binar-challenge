'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('player_achievements', [
      {
        player_id: 1,
        achievement_id:1,
        createdAt: new Date(),
        updatedAt: new Date()
      },    {
        player_id: 2,
        achievement_id:2,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('player_achievements', null, { truncate: true, restartIdentity: true });
    /**
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
