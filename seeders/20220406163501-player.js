'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('players', [
      {
        usernames: "dummyUser",
        password: bcrypt.hashSync('dummyPass', 10),
        email:"dummy@example.com",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        usernames: "dummyPlayer",
        password: bcrypt.hashSync('dummyPassw', 10),
        email:"Baka@example.com",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  
  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('players', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
