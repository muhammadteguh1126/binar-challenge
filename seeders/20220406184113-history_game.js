'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('history_games', [
      {
        score: 10,
        history_game: 'lose',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        score: 11,
        history_game: 'win',
        createdAt: new Date(),
        updatedAt: new Date()
      },
   ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('history_games', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
