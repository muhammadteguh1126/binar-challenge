'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
   return queryInterface.bulkInsert('match_games', [
      {
        player_id: 1,
        history_id:1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        player_id: 2,
        history_id:2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
   ])
  },
  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('match_games', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
