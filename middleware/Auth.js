require("dotenv").config();
const {player} = require("../models")
const jwt = require('jsonwebtoken');
const { JWT_SECRET } =  process.env

module.exports = async(req, res, next) => {
  try {
      const authHeader = req.header('Authorization');
      if (!authHeader) {
          throw {
              status: 401,
              message: 'Authorization denied'
          }
      }
      const token = authHeader;
      let player;
      player = jwt.verify(token, JWT_SECRET);
    //   console.log(token)
      if (!player) {
          throw {
              status: 401,
              message: 'Invalid token'
            }
        }else{
            req.PlayerId = player.id
            next();
        }
  } catch (error) {
      next(error);
  }
}