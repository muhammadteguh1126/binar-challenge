const { body, validationResult } = require("express-validator")

const validate = (req, res, next) => {
    try {
      const errors = validationResult(req)
      if (errors.isEmpty()) {
        return next();
      } else {
        const extractedErrors = [];
        errors
          .array()
          .map((err) => extractedErrors.push({ [err.param]: err.msg }));
  
        throw {
          status: 422,
          message: extractedErrors,
        }
      }
    } catch (error) {
      next(error)
    }
  }

  const CreateRules = () => {
    return [
      body("usernames", "Usernames is required").notEmpty(),
      body("email", "Must Be a valid email address").isEmail(),
    ]
  }

  module.exports = {
    validate,
    CreateRules
}